package com.kumar.raman.shrikrishan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kumar.raman.shrikrishan.Activity.GalleryActivity;
import com.kumar.raman.shrikrishan.Activity.GeetaActivity;
import com.kumar.raman.shrikrishan.Activity.NewGalleryActivity;
import com.kumar.raman.shrikrishan.Activity.VideoActivity;
import com.kumar.raman.shrikrishan.Activity.WallpaperActivity;
import com.kumar.raman.shrikrishan.Pojo.AudioModel;
import com.kumar.raman.shrikrishan.networking.ImageFilePath;
import com.kumar.raman.shrikrishan.networking.MyFcmListenerService;
import com.kumar.raman.shrikrishan.networking.NetworkingCallbackInterface;
import com.kumar.raman.shrikrishan.networking.RequestHandler;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RecyclerView recyclerView;
    private ImagesAdapter mAdapter;
    private List<Integer> imagesList = new ArrayList<>();
    Spinner selctOptions;
    ImageView uploadImage,uploadAudio,uploadVideo;
    ImageView imagePreview;
    private static final int PERMISSION_REQUEST_CAMERA = 0;
    private static final int PERMISSION_REQUEST_AUDIO = 5;
    private static final int PERMISSION_REQUEST_STORAGE = 2;
    private static final int REQUEST_CAMERA = 1000;
    private static final int SELECT_FILE = 2000;
    static final int CAMERA_PIC_REQUEST = 1000;
    static final int GALLERY_PIC_REQUEST = 2000;
    static final int VIDEO_CAPTURE = 4000;
    String mCurrentPhotoPath;
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;
    public ArrayList<String> imageURLList = new ArrayList<>();
    private static final String TAG = "LoginActivity";
    public static boolean loginFlag=false;
    ProgressDialog progress;
    String refreshedToken =null;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView backImage=(ImageView)findViewById(R.id.backImage);
        setSupportActionBar(toolbar);
        mAuth = FirebaseAuth.getInstance();
        progress = new ProgressDialog(this);
        FirebaseApp.initializeApp(this);
        registerFCM();
        LinearLayout lay1=(LinearLayout) findViewById(R.id.lay1);
        LinearLayout lay2=(LinearLayout) findViewById(R.id.lay2);
        LinearLayout lay3=(LinearLayout) findViewById(R.id.lay3);
        LinearLayout lay4=(LinearLayout) findViewById(R.id.lay4);

        LinearLayout lay5=(LinearLayout) findViewById(R.id.lay5);
        LinearLayout lay6=(LinearLayout) findViewById(R.id.lay6);
        LinearLayout lay7=(LinearLayout) findViewById(R.id.lay7);
        LinearLayout lay8=(LinearLayout) findViewById(R.id.lay8);
        LinearLayout lay9=(LinearLayout) findViewById(R.id.lay9);
        setTitle(getString(R.string.app_name));
        // To maintain FB Login session
        lay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, NewAudioActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
                }
            }
        });
        lay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, ImageActivity.class);
                    startActivity(i);
                }else{
                Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
            }
            }
        });
        lay3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected()){
                    Intent i = new Intent(MainActivity.this, WallpaperActivity.class);
                startActivity(i);
            }else{
                Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
            }

            }
        });
        lay4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()){
                Intent i=new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_SUBJECT,"Share App");
                i.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.kumar.raman.shrikrishan");
                startActivity(Intent.createChooser(i,"Share via"));
                }else{
                    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
                }
            }
        });
        lay5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()) {
                    Intent i = new Intent(MainActivity.this, TextActivity.class);
                    startActivity(i);
                }else{
                Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
            }
            }
        });
        lay6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()){
                Intent i=new Intent(MainActivity.this,GeetaActivity.class);
                startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
                }
            }
        });
        lay7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()){
                    Intent i=new Intent(MainActivity.this,GalleryActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
                }
            }
        });
        lay8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkConnected()){
                    Intent i=new Intent(MainActivity.this,NewGalleryActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
                }
            }
        });

        lay9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkConnected()){
                    Intent i=new Intent(MainActivity.this,VideoActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }else{
                    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Menu menuNav = navigationView.getMenu();

      final MenuItem loginItem = menuNav.findItem(R.id.login);
      final MenuItem logoutItem = menuNav.findItem(R.id.log_out);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {
                if(loginFlag){
                    loginItem.setVisible(false);
                    logoutItem.setVisible(true);
                }else{
                    loginItem.setVisible(true);
                    logoutItem.setVisible(false);
                }

            }

            @Override
            public void onDrawerClosed(View view) {

            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });
        toggle.syncState();

        checkVersion();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @SuppressLint("NewApi")
    public Bitmap transform(Bitmap bitmap) {
        // Create another bitmap that will hold the results of the filter.
        Bitmap blurredBitmap = Bitmap.createBitmap(bitmap);
        android.renderscript.RenderScript rs = android.renderscript.RenderScript.create(getApplicationContext());
        try {
            // Allocate memory for Renderscript to work with
            android.renderscript.Allocation input = android.renderscript.Allocation.createFromBitmap(rs, bitmap, android.renderscript.Allocation.MipmapControl.MIPMAP_FULL,
                    android.renderscript.Allocation.USAGE_SHARED);
            android.renderscript.Allocation output = android.renderscript.Allocation.createTyped(rs, input.getType());
            // Load up an instance of the specific script that we want to use.
            android.renderscript.ScriptIntrinsicBlur script = android.renderscript.ScriptIntrinsicBlur.create(rs, android.renderscript.Element.U8_4(rs));
            script.setInput(input);
            // Set the blur radius
            script.setRadius(10);
            // Start the ScriptIntrinisicBlur
            script.forEach(output);
            // Copy the output to the blurred bitmap
            output.copyTo(blurredBitmap);
            // bitmap.recycle();
        } catch (Exception ex) {
            System.out.println("error in userprofilefragment class :" + ex);
        }
        return blurredBitmap;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_gallery) {
            imagesList.clear();

            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            return true;
        }
        if (id == R.id.action_gallery1) {
            imagesList.clear();
           // imagesList.add(R.drawable.image2);
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.login) {
            // Handle the camera action
            if(!loginFlag) {
                showLoginDialog();
            }else{
                        Toast.makeText(getApplicationContext(), "Already login",
                Toast.LENGTH_SHORT).show();

            }

        }else if (id == R.id.contact_us) {
showContactUsDialog();
        }

        else if (id == R.id.log_out) {
            if(loginFlag) {
                signOut();
                Toast.makeText(getApplicationContext(), "Log out successfully",
                Toast.LENGTH_SHORT).show();
                loginFlag=false;
            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void loadPaymentMode(final ArrayList<String> list) {
        // Spinner Drop down elements
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_lay, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selctOptions.setAdapter(dataAdapter);

        selctOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void showAudioPreview() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview

        } else {
            // Permission is missing and must be requested.
            requestAudioPermission();
        }
    }




    private void requestAudioPermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.RECORD_AUDIO)) {
            requestPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_AUDIO);

        } else {
            requestPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_AUDIO);
        }
    }
    public  void showLoginDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.login_dialog_layout);


        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button okButton = (Button) dialog.findViewById(R.id.okButton);
       final EditText userName=(EditText)dialog.findViewById(R.id.userName);
      final  EditText password=(EditText)dialog.findViewById(R.id.password);
       final TextView errorView=(TextView)dialog.findViewById(R.id.errorView);
        Button cancelButton=(Button) dialog.findViewById(R.id.cancelButton);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                errorView.setVisibility(View.GONE);
                signIn(userName.getText().toString(),password.getText().toString(),errorView,dialog);
            }
        });
        dialog.show();
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }


    public void createDailogBox() {
        CharSequence colors[] = new CharSequence[]{"Camera",
                "Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select an Options");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {


                if (pos == 0) {
                    showCameraPreview();
                } else if (pos == 1) {
                    showStoragereview();
                }
            }
        });
        builder.show();

    }
    private void showCameraPreview() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview
            startCamera();
        } else {
            // Permission is missing and must be requested.
            requestCameraPermission();
        }
    }

    private void requestCameraPermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CAMERA);

        } else {
            requestPermissions(new String[]{android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CAMERA);
        }
    }


    public void startCamera() {
        File f = null;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            f = setUpPhotoFile();
            mCurrentPhotoPath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        } catch (IOException e) {
            e.printStackTrace();
            f = null;
            mCurrentPhotoPath = null;
        }
        startActivityForResult(takePictureIntent, REQUEST_CAMERA);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath() + "/MTCollect/images/profile");

        dir.mkdirs();
        // Create a name for the saved image
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        String saveImageName = "profile_" +/*UserDAO.getInstance(getActivity()).getUserId()*/currentDateandTime + ".jpg";


        File file = new File(dir, saveImageName);
        return file;
    }
    private void showStoragereview() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview
            startGallery();
        } else {
            // Permission is missing and must be requested.
            requestStoragePermission();
        }
    }

    private void requestStoragePermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);

        } else {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_STORAGE);
        }
    }

    public void startGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }
    public String uploadData(String imageCategory, File file, String fileName, String url) {
        String imageUploadResponse = "msg";
        try {
            UploadDownloadFileClient client = new UploadDownloadFileClient(
                    url);
            //client.setmProgressListener(getApplicationContext());
            client.connectForMultipart();
            client.addFormPart("requestType", "upload");
            //client.addFormPart("localId", uniqueId);
            if (imageCategory.equalsIgnoreCase("receipt") || imageCategory.equalsIgnoreCase("cheque") || imageCategory.equalsIgnoreCase("others")) {
                client.addFormPart("imageCategory", imageCategory);
            }
            client.addDocumentFilePart("file",
                    (fileName), file);
            client.finishMultipart();
            imageUploadResponse = client.getResponse();
            System.out.println("Upload response " + imageUploadResponse);
        } catch (Exception e) {
            e.printStackTrace();
            imageUploadResponse = "msg";
        }
        return imageUploadResponse;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bm = null;
        if (requestCode == CAMERA_PIC_REQUEST && resultCode == RESULT_OK) {
            // bm = (Bitmap) data.getExtras().get("data");

            if (mCurrentPhotoPath != null) {
                bm = getBitmapWithOrientation(mCurrentPhotoPath, 768);
                mCurrentPhotoPath = null;
            }
            imageURLList.add(saveFinalImage(bm));
            if(imagePreview!=null){
                imagePreview.setImageBitmap(bm);
            }

        } else if (requestCode == GALLERY_PIC_REQUEST && resultCode == RESULT_OK
                && null != data) {

            InputStream is = null;
            try {
                String realPath = ImageFilePath.getPath(this, data.getData());
                Bitmap bitmap = getBitmapWithOrientation(realPath, 768);

                imageURLList.add(saveFinalImage(bitmap));
                if(imagePreview!=null){
                    imagePreview.setImageBitmap(bm);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
    public static Bitmap getBitmapWithOrientation(String path, int newSize) {
        Bitmap myBitmap = BitmapFactory.decodeFile(path);

        try {
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            int width = myBitmap.getWidth();
            int height = myBitmap.getHeight();
            if (newSize != 0) {
                if (height > width) {
                    if (width > newSize) {
                        float ratio = (float) height / (float) width;
                        width = newSize;
                        height = (int) ((float) width * ratio);
                    }
                } else {
                    if (height > newSize) {
                        float ratio = (float) width / (float) height;
                        height = newSize;
                        width = (int) ((float) height * ratio);
                    }
                }
            }
            myBitmap = Bitmap.createScaledBitmap(myBitmap, width, height, false);
            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myBitmap;
    }
    public String saveFinalImage(Bitmap bm) {
        // Find the SD Card path
        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        // getAbsolutePath for sd card
        File dir;
        dir = new File(filepath.getAbsolutePath() + "/MTCollect/images/others");


        dir.mkdirs();
        // Create a name for the saved image
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        String saveImageName = "";
        saveImageName = "" + (imageURLList.size() + 1) + ".jpg";


        File file = new File(dir, saveImageName);

        // Show a toast message on successful save
        /*Toast.makeText(getApplicationContext(), "Image Saved to SD Card",
                Toast.LENGTH_SHORT).show();*/
        try {

            OutputStream output = new FileOutputStream(file);

            //  Bitmap bm = cropBorderFromBitmap(finalImg);

            // Compress into png format image from 0% - 100%
            bm.compress(Bitmap.CompressFormat.JPEG, 50, output);
            output.flush();
            output.close();

            refreshGallery(file);
            //	MediaScannerConnection.scanFile(this, new String[] { dir.getPath() }, new String[] { "image/jpeg" }, null);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return dir + "/" + saveImageName;

    }
    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }
    public  void showCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.success_error_view1);
         imagePreview=(ImageView)dialog.findViewById(R.id.imagePreview);

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button okButton = (Button) dialog.findViewById(R.id.okButton);
       Button cancelButton=(Button) dialog.findViewById(R.id.cancelButton);
        imagePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                createDailogBox();
                // dialog.dismiss();
            }
        });
        dialog.show();
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public void showProgressDialog(){

        progress.setMessage("Login In ....");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        //Todo getdata
    }
    private void signIn(String email, String password,final TextView errorView,final Dialog dialog) {
        Log.e(TAG, "signIn:" + email);
        if (!validateForm(email, password)) {
            return;
        }
        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.e(TAG, "signIn: Success!");
                            progress.dismiss();
                            // update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            errorView.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, " Login Successful!", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            loginFlag=true;
                        } else {
                            Log.e(TAG, "signIn: Fail!", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed!", Toast.LENGTH_SHORT).show();
                            errorView.setVisibility(View.VISIBLE);
                            loginFlag=false;
                            progress.dismiss();
                        }

                        if (!task.isSuccessful()) {
                          errorView.setVisibility(View.VISIBLE);                        }
                           }
                });
    }

    private void signOut() {
        mAuth.signOut();
    }

    private boolean validateForm(String email, String password) {

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(MainActivity.this, "Enter email address!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(MainActivity.this, "Enter password!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.length() < 6) {
            Toast.makeText(MainActivity.this, "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    public  void showContactUsDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.contact_us_lay);


        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button okButton = (Button) dialog.findViewById(R.id.okButton);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
          dialog.dismiss();
            }
        });
        dialog.show();

    }


    public void checkVersion(){
        {
            JSONObject json =new JSONObject();
            String url="http://ramankumarynr.com/api/?json=get_post&id=1609";
          //  showProgressDialog();
            RequestHandler.getAllAudios(url,this,json, new NetworkingCallbackInterface() {
                @Override
                public void onSuccess(NetworkResponse response, boolean fromCache) {
                    System.out.print("response........"+response);
                 //   progress.dismiss();
                    try {
                        JSONArray jsonArray=new JSONArray(response);
                        System.out.print("jsonArrayresponse........"+jsonArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onSuccess(String response, boolean fromCache) {
                    System.out.print("response........"+response);
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        String status=jsonObject.getString("status");
if(status.equalsIgnoreCase("ok")){
    showDialog();
}


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(VolleyError error) {
                    //progress.dismiss();
                  //  Toast.makeText(MainActivity.this, "Connection not available",Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onNetworkFailure(String error) {
                   // progress.dismiss();
                    Toast.makeText(MainActivity.this, "Internet connection not available",Toast.LENGTH_SHORT).show();

                }
            });
        }
    }
   public void showDialog(){

       final Dialog dialog = new Dialog(this);
       dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       dialog.setContentView(R.layout.contact_us_lay);
TextView titleText=(TextView)dialog.findViewById(R.id.titleText);
TextView errorView=(TextView)dialog.findViewById(R.id.errorView);
errorView.setText("A new version has been released. Please update a latest version from play store");

       dialog.getWindow().setBackgroundDrawable(
               new ColorDrawable(android.graphics.Color.TRANSPARENT));
       Button okButton = (Button) dialog.findViewById(R.id.okButton);
titleText.setText("Update");
       okButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View arg0) {
               // TODO Auto-generated method stub
               dialog.dismiss();
           }
       });
       dialog.show();
    }
    public void registerFCM(){
         refreshedToken = FirebaseInstanceId.getInstance().getToken();
        System.out.println("device id is ==>>" + refreshedToken);
    }

    public void saveToken(String token,String id) {
        JSONObject json = new JSONObject();
        try {
            json.put("token", token);
            json.put("key", getMd5("ramankumarynr"));
            System.out.print("token.........#######"+token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestHandler.saveToken( this,token,getMd5("ramankumarynr"), json, new NetworkingCallbackInterface() {
            @Override
            public void onSuccess(NetworkResponse response, boolean fromCache) {
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    System.out.print(jsonArray);
                    SharedPreferences.Editor editor = getSharedPreferences("token", MODE_PRIVATE).edit();
                    editor.putBoolean("status",true);
                    editor.apply();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onSuccess(String response, boolean fromCache) {
                try {
                    Toast.makeText(getApplicationContext(), response,Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = getSharedPreferences("token", MODE_PRIVATE).edit();
                    editor.putBoolean("status",true);
                    editor.apply();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Connection not available",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNetworkFailure(String error) {
                Toast.makeText(getApplicationContext(), "Internet Connection not available",Toast.LENGTH_SHORT).show();

            }
        });
    }
    public static String getMd5(String input)
    {
        try {
            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());
            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);
            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onResume() {
        if(refreshedToken!=null){
            String androidId = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            SharedPreferences prefs = getSharedPreferences("token", MODE_PRIVATE);
            boolean tokenStatus= prefs.getBoolean("status", false);
            if (!tokenStatus) {
                saveToken(refreshedToken,androidId);
            }
//            MyFcmListenerService myFcmListenerService=new MyFcmListenerService();
//            myFcmListenerService.sendMyNotification("2 images uploaded");
        }
        super.onResume();

    }
}
