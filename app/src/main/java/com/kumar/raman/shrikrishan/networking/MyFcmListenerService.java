package com.kumar.raman.shrikrishan.networking;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kumar.raman.shrikrishan.MainActivity;
import com.kumar.raman.shrikrishan.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

/**
 * Created by lakshay- on 3/14/2019.
 */

public class MyFcmListenerService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage message) {
      //  Log.d(TAG, "From: " + message.getFrom());
     //   Log.d(TAG, "Notification Message Body: " + message.getNotification().getBody());
        //String body=message.getNotification().getIcon();
        Map<String, String> data = message.getData();


        String bodyMsg="";
        String image_url="";
        String title="";
        try {

 if(message.getData().size()>0){
    bodyMsg = data.get("message");
    image_url = (String)data.get("image");
    title = data.get("title");
}else if(message.getNotification()!=null){
         //JSONObject json=new JSONObject(body);
         bodyMsg= message.getNotification().getBody();
         image_url= message.getNotification().getIcon();
         title= message.getNotification().getTitle();
     }


        } catch (Exception e) {
            e.printStackTrace();
        }

        sendMyNotification(bodyMsg,title,image_url);

    }


    public void sendMyNotification(String message,String title,String image_url) {
       Bitmap bm=null;
        if(image_url!=null&&!image_url.equalsIgnoreCase("")) {
             bm = loadBitmap(image_url);
        }
        //On click of notification it redirect to this Activity
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//        Bitmap icon = BitmapFactory.decodeResource(getResources(),
//                R.drawable.image3);
        NotificationChannel mChannel = null;
        String idChannel = "01";
        // The id of the channel.

        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationCompat.BigPictureStyle bpStyle = new NotificationCompat.BigPictureStyle();
        bpStyle.bigPicture(bm).build();

        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.cust_push);
       // contentView.setImageViewResource(R.id.image, bm);
        contentView.setImageViewBitmap(R.id.image, bm);
        contentView.setImageViewBitmap(R.id.finalImage, bm);
        contentView.setTextViewText(R.id.title, title);
        contentView.setTextViewText(R.id.text, message);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_ac_unit)
//                .setCustomBigContentView(contentView)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        if(bm!=null){
            notificationBuilder.setLargeIcon(bm);
            notificationBuilder.setStyle(bpStyle);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(idChannel, getApplicationContext().getString(R.string.app_name), importance);
            // Configure the notification channel.
            mChannel.setDescription("Upload");
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(mChannel);
        }


        notificationManager.notify(0, notificationBuilder.build());
    }
    public Bitmap loadBitmap(String url)
    {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try
        {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (bis != null)
            {
                try
                {
                    bis.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }
}
