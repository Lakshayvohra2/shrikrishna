package com.kumar.raman.shrikrishan.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.kumar.raman.shrikrishan.Pojo.ImagesData;
import com.kumar.raman.shrikrishan.R;
import com.kumar.raman.shrikrishan.ShareImageFragment;
import com.kumar.raman.shrikrishan.networking.NetworkingCallbackInterface;
import com.kumar.raman.shrikrishan.networking.RequestHandler;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.kumar.raman.shrikrishan.Activity.WallpaperActivity.DownloadFileFromURL.progress_bar_type;


public class WallpaperActivity extends AppCompatActivity  implements WallpaperAdapter.ShareFacebook{
    RecyclerView recyclerView;
    private WallpaperAdapter mAdapter;
    private List<ImagesData> imagesList = new ArrayList<>();
    private StorageReference mStorageRef;
    ProgressDialog progress;
    ShareDialog shareDialog;
    private CallbackManager callbackManager;
    private ProgressDialog finalProgressDialog;
    private static String TAG = ShareImageFragment.class.getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallpaper);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Shri Krishan Amrit");
        mStorageRef = FirebaseStorage.getInstance().getReference();
//        mAdapter = new WallpaperAdapter(imagesList,getApplicationContext(),getSupportFragmentManager().beginTransaction());

//        recyclerView.setAdapter(mAdapter);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        progress = new ProgressDialog(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
//
//        // Create a callbackManager to handle the login responses.
        callbackManager = CallbackManager.Factory.create();
//        shareDialog = new ShareDialog(this);
//        shareDialog.registerCallback(callbackManager, callback);

        getAllImages(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void getAllImages(final WallpaperAdapter.ShareFacebook shareFacebook) {

        JSONObject json =new JSONObject();
        showProgressDialog();

        RequestHandler.getAllImages(this,json, new NetworkingCallbackInterface() {
            @Override
            public void onSuccess(NetworkResponse response, boolean fromCache) {
                System.out.print("response........"+response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    System.out.print("jsonArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(String response, boolean fromCache) {
                System.out.print("response........"+response);
                progress.dismiss();
                    try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("posts");
                    ImagesData content=new ImagesData();
                    for(int i=0;i<jsonArray.length();i++){
                        content=new ImagesData();
                        JSONObject json= jsonArray.getJSONObject(i);
                        JSONObject thumbnailJson=json.getJSONObject("thumbnail_images");
                        String title=json.getString("content");

                        JSONObject fullJSON= thumbnailJson.getJSONObject("full");

                        String url= fullJSON.getString("url");
                        content.setThumbnal(title);
                        content.setFullImage(url);
                        imagesList.add(content);
                    }
                    mAdapter = new WallpaperAdapter(imagesList,WallpaperActivity.this,getSupportFragmentManager()
                            .beginTransaction(),getSupportFragmentManager());
                        mAdapter.setImageListner(shareFacebook);
                    recyclerView.setAdapter(mAdapter);
                    System.out.print("jsonObjectArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(VolleyError error) {
            }

            @Override
            public void onNetworkFailure(String error) {
            }
        });
    }
    public void showProgressDialog(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }

    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            Log.v(TAG, "Successfully posted");
            // Write some code to do some operations when you shared content successfully.
        }

        @Override
        public void onCancel() {
            Log.v(TAG, "Sharing cancelled");
            // Write some code to do some operations when you cancel sharing content.
        }

        @Override
        public void onError(FacebookException error) {
            Log.v(TAG, error.getMessage());
            // Write some code to do some operations when some error occurs while sharing content.
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Call callbackManager.onActivityResult to pass login result to the LoginManager via callbackManager.
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void shareImage(String image,String title,ProgressDialog newProgressDialog) {

        Bundle bundle=new Bundle();
        bundle.putString("image",image);
        bundle.putString("title",title);

        Fragment fragment = new ShareImageFragment();
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.wallcontainer, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
       // newProgressDialog.dismiss();
    }
    @Override
    public ProgressDialog showProgress() {
        ProgressDialog dialog=new ProgressDialog(this);
        //  progressDialog.setTitle("Loading");
        dialog.setMessage("Redirecting to facebook.");
        // progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        dialog.show();
        finalProgressDialog=dialog;
        return dialog;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(finalProgressDialog!=null){
            finalProgressDialog.dismiss();
        }
    }

    @Override
    public void dismissProgressDialog(ProgressDialog progressDialog) {
        progressDialog.dismiss();
    }

    @Override
    public ProgressDialog showDialoadingProgress() {

        return null;
    }

    @Override
    public void downloadImageFromURL(String image,String title) {
        new DownloadFileFromURL().execute(image,title);
    }
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        public static final int progress_bar_type = 0;
        private ProgressDialog pDialog;
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
            pDialog=showDownloadingProgress(pDialog);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String title=f_url[1];
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/srikrishna/images");

                if (!myDir.exists()) {
                    myDir.mkdirs();
                }

                String name = new Date().toString() + ".jpg";
                myDir = new File(myDir, name);

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(myDir);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(myDir.getAbsolutePath(),bmOptions);
                MediaStore.Images.Media.insertImage(getContentResolver(),bitmap , title , title);
                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String myDir) {
            // dismiss the dialog after the file was downloaded
           //dismissDialog(progress_bar_type);
           // File file=new File(myDir);

            pDialog.setProgress(100);
            pDialog.setMessage("Download Completed");
            pDialog.setCancelable(true);
            try {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismiss();
                    }
                }, 2000);
            }catch (Exception e){
                e.printStackTrace();

            }
            //pDialog.dismiss();
            // Displaying downloaded image into image view
            // Reading image path from sdcard
          //  String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            // setting downloaded into image view
           // my_image.setImageDrawable(Drawable.createFromPath(imagePath));
        }

    }
    public ProgressDialog showDownloadingProgress(ProgressDialog dialog){
         dialog=new ProgressDialog(this);
        //  progressDialog.setTitle("Loading");
        dialog.setMessage("Downloading..");
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCancelable(false);
        // progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        dialog.show();
        return dialog;
    }

}
