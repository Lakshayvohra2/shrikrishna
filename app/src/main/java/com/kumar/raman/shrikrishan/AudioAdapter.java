package com.kumar.raman.shrikrishan;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kumar.raman.shrikrishan.Pojo.AudioModel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.support.v4.content.FileProvider.getUriForFile;

/**
 * Created by mann on 9/2/18.
 */

class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.MyViewHolder> {
    FragmentTransaction ft;
    private List<AudioModel> audioList;
    Context mContext;
    ArrayList<Integer> resID;
    private final ClickListener listener;
    MediaPlayer mediaPlayer;
    private String fNmae = "ring1.mp3";
    private String fPAth = "android.resource://com.example.mann.myapplication/raw/ring1";
    CallAudioField callAudioField;

    private String downloadAudioPath;
    private String urlDownloadLink = "";
    public int ringtoneStatus=0;
    ProgressDialog newProgressDialog;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        LinearLayout textview1;
        LinearLayout stop, alarm_icon;
        RelativeLayout main_layout;
        LinearLayout play_icon;
        TextView text;
        private WeakReference<ClickListener> listenerRef;

        private TextAdapter.ItemClickListener clickListener;

        public MyViewHolder(View view, ClickListener listener) {
            super(view);
            listenerRef = new WeakReference<>(listener);
            textview1 = (LinearLayout) view.findViewById(R.id.textview1);
            main_layout = (RelativeLayout) view.findViewById(R.id.main_layout);
            stop = (LinearLayout) view.findViewById(R.id.stop);
            alarm_icon = (LinearLayout) view.findViewById(R.id.alarm_icon);
            play_icon = (LinearLayout) view.findViewById(R.id.play_icon);
            text=(TextView)view.findViewById(R.id.text);
            stop.setOnClickListener(this);
            alarm_icon.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

            if (v.getId() == alarm_icon.getId()) {


            } else {

            }

            listenerRef.get().onPositionClicked(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    public void setOnCallAudioFieldListener(CallAudioField callAudioField) {
        this.callAudioField = callAudioField;
    }

    public AudioAdapter(List<AudioModel> imagesList, Context context, FragmentTransaction ft, ArrayList<Integer> resID, MediaPlayer mediaPlayer, ClickListener listener) {
        this.audioList = imagesList;
        this.mContext = context;
        this.ft = ft;
        this.resID = resID;
        this.listener = listener;
        this.mediaPlayer = mediaPlayer;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.audio_list_row, parent, false);

        return new MyViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final String txt = audioList.get(position).getTitle();
        String url = audioList.get(position).getUrl();
        holder.text.setText(txt);
        holder.textview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callAudioField instanceof CallAudioField) {
                    callAudioField.showPlayDialog(position);
                }
                //  callAudioField.showPlayDialog(position);
            }
        });
        holder.play_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callAudioField instanceof CallAudioField) {
                    callAudioField.showPlayDialog(position);
                }
            }
        });
        holder.alarm_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ringtoneStatus=1;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.System.canWrite(mContext)) {
// Do stuff here
                        if(isStoragePermissionGranted()) {
                            downloadFile(position, "alarm");
                        }
                    }
                    else {
                        if (isStoragePermissionGranted()) {
                            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                          //  intent.setData(Uri.parse("package:" + mContext.getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }
                    }
                }


              //  downloadFile(position,"alarm");
            }
        });
        holder.stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(v.getContext(), "SET AS RINGTONE = " + String.valueOf(position), Toast.LENGTH_SHORT).show();
                // setRingtone("ringtone");
                ringtoneStatus=2;
                //  setRingtone(downloadAudioPath,filename);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.System.canWrite(mContext)) {
// Do stuff here
if(isStoragePermissionGranted()) {
    downloadFile(position, "ringtone");
}
                    }
                     else {
                        if(isStoragePermissionGranted()) {
                            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }
                    }
                }
            }
        });

    }

    public  boolean isStoragePermissionGranted() {
        boolean status=false;
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (mContext.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Permission is granted");
                    status=true;
                    return status;
                } else {

                    Log.v(TAG, "Permission is revoked");
                    ActivityCompat.requestPermissions((NewAudioActivity) mContext, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    status=false;
                    return status;
                }
            } else { //permission is automatically granted on sdk<23 upon installation
                Log.v(TAG, "Permission is granted");
                status=true;
                return status;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return status;
    }

public  void downloadFile(int position,String type){
    urlDownloadLink = audioList.get(position).getUrl();
//    String sdcard = Environment.getExternalStorageState();
//
//    if(sdcard.equals(Environment.MEDIA_MOUNTED)){
//        downloadAudioPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath();
//    } else {
//        downloadAudioPath = Environment.getRootDirectory().getAbsolutePath();
//    }
    File audioVoice = new File(mContext.getExternalFilesDir(null), "audio");


   // File audioVoice = new File(imagePath, "sriKrishna");
    if (!audioVoice.exists()) {
        audioVoice.mkdir();
    }
    String filename = extractFilename();
    File newFile = new File(audioVoice, filename);
    downloadAudioPath=newFile.getAbsolutePath();
    System.out.print("downloadAudioPath"+downloadAudioPath);
    //downloadAudioPath = downloadAudioPath + File.separator + "sriKrishna/audio/"+filename;
    DownloadFile downloadAudioFile = new DownloadFile();
    //progressDialog.show();
    // showProgressDialog();
    // progressDialog=new ProgressDialog(mContext);
    newProgressDialog= callAudioField.showProgress();
    downloadAudioFile.execute(urlDownloadLink, downloadAudioPath,filename);
}
    public interface CallAudioField {
        public void play(int positon);
        public void pause();
        public void showPlayDialog(int position);
        public ProgressDialog showProgress();
        public void dismissProgressDialog(ProgressDialog progressDialog);
    }

    @Override
    public int getItemCount() {
        return audioList.size();
    }

    public void setNewRing(String path, String file) {
        File k = new File(path); // path is a file to /sdcard/media/ringtone
        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, k.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, file);
        values.put(MediaStore.MediaColumns.SIZE, 215454);
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
        values.put(MediaStore.Audio.Media.ARTIST, "Madonna");
        values.put(MediaStore.Audio.Media.DURATION, 230);
        if(ringtoneStatus==1){
            values.put(MediaStore.Audio.Media.IS_ALARM, true);
            values.put(MediaStore.Audio.Media.IS_RINGTONE, false);
        }else if(ringtoneStatus==2){
            values.put(MediaStore.Audio.Media.IS_ALARM, false);
            values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        }
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);
//        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//        //Insert it into the database
//        Uri parcialUri = MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath());
//       // mContext.getContentResolver().delete(parcialUri, MediaStore.MediaColumns.DATA + "=\"" + file.getAbsolutePath() + "\"", null);
//        int res = mContext.getContentResolver().update(musicUri, values, android.provider.MediaStore.Audio.Media._ID + "=?", new String[] { String.valueOf(id) });
//        Uri newUri =mContext.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath()), values);
        //callAudioField.dismissProgressDialog(progressDialog);

        Uri parcialUri = MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath());
        Uri newUri =null;
        try {
          //  Uri parcialUri = FileProvider.getUriForFile(mContext, "com.kumar.raman.shrikrishan.fileprovider", k);
  //Uri parcialUri = MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath());
//updateFile(k,values);
            mContext.getContentResolver().delete(parcialUri, MediaStore.MediaColumns.DATA + "=\"" + k.getAbsolutePath() + "\"", null);
             newUri = mContext.getContentResolver().insert(parcialUri, values);

        }catch (Exception e){
            e.printStackTrace();
        }
        try {


            if(ringtoneStatus==1){
                RingtoneManager.setActualDefaultRingtoneUri(
                        mContext,
                        RingtoneManager.TYPE_ALARM,
                        newUri);
                newProgressDialog.dismiss();
                if ((RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_ALARM)).equals(newUri)) {

                    Toast.makeText(mContext, new StringBuilder().append("Alarm set successfully"), Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(mContext, new StringBuilder().append("Failed"), Toast.LENGTH_LONG).show();
                }
            }else if(ringtoneStatus==2){
                RingtoneManager.setActualDefaultRingtoneUri(
                        mContext,
                        RingtoneManager.TYPE_RINGTONE,
                        newUri);
                newProgressDialog.dismiss();
                if ((RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE)).equals(newUri)) {
                    Toast.makeText(mContext, new StringBuilder().append("Ringtone set successfully"), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(mContext, new StringBuilder().append("Failed"), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            newProgressDialog.dismiss();
            Toast.makeText(mContext, new StringBuilder().append("Ringtone/Alarm failed"), Toast.LENGTH_LONG).show();

            e.printStackTrace();
        }

    }




    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Images.Media._ID },
                MediaStore.Images.Media.DATA + "=? ",
                new String[] { filePath }, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            cursor.close();
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }
       public void setRingtone(String path,String fileName){

       File file = new File(path, fileName);
       if (!file.getParentFile().exists()) {
           file.getParentFile().mkdirs();
       }
       if (!file.exists()) {
           try {
               file.createNewFile();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }
       Uri parse = Uri.parse(file.toString());
       AssetFileDescriptor openAssetFileDescriptor;
       ContentResolver contentResolver = mContext.getContentResolver();

       ContentValues contentValues = new ContentValues();
       contentValues.put("_data", file.getAbsolutePath());
       contentValues.put("title", "nkDroid ringtone");
       contentValues.put("mime_type", "audio/mp3");
       contentValues.put("_size", Long.valueOf(file.length()));
       contentValues.put("artist", Integer.valueOf(R.string.app_name));
           if(ringtoneStatus==1){
               contentValues.put("is_alarm", Boolean.valueOf(true));
               contentValues.put("is_ringtone", Boolean.valueOf(false));
           }else if(ringtoneStatus==2){
               contentValues.put("is_alarm", Boolean.valueOf(false));
               contentValues.put("is_ringtone", Boolean.valueOf(true));
           }
          // contentValues.put("is_alarm", Boolean.valueOf(false));
           contentValues.put("is_ringtone", Boolean.valueOf(true));



       contentValues.put("is_notification", Boolean.valueOf(false));

       contentValues.put("is_music", Boolean.valueOf(false));

       try {


           RingtoneManager.setActualDefaultRingtoneUri(mContext, 1, contentResolver.insert(MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath()), contentValues));
           if(ringtoneStatus==1){
               Toast.makeText(mContext, new StringBuilder().append("Alarm Tone set successfully"), Toast.LENGTH_LONG).show();

           }else if(ringtoneStatus==2){
               Toast.makeText(mContext, new StringBuilder().append("Ringtone set successfully"), Toast.LENGTH_LONG).show();

           }
       } catch (Throwable th) {
           Toast.makeText(mContext, new StringBuilder().append("Ringtone feature is not working"), Toast.LENGTH_LONG).show();
       }
   }
    private class DownloadFile extends AsyncTask<String, Integer, String> {
        String filename="";
        @Override
        protected String doInBackground(String... url) {
            int count;
            filename=url[2];

            try {
                URL urls = new URL(url[0]);
                URLConnection connection = urls.openConnection();
                connection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = connection.getContentLength();

                InputStream input = new BufferedInputStream(urls.openStream());
                OutputStream output = new FileOutputStream(url[1]);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                   // publishProgress("" + (int) ((total * 100) / lengthOfFile));

                    publishProgress((int) (total * 100 / lenghtOfFile));
                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lenghtOfFile));


                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // progressbar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // progressbar.setVisibility(ProgressBar.GONE);
            setNewRing(downloadAudioPath, filename);
           // setRingtone(downloadAudioPath, filename);
        }
    }

    private String extractFilename() {
        if (urlDownloadLink.equals("")) {
            return "";
        }
        String newFilename = "";
        if (urlDownloadLink.contains("/")) {
            int dotPosition = urlDownloadLink.lastIndexOf("/");
            newFilename = urlDownloadLink.substring(dotPosition + 1, urlDownloadLink.length());
        } else {
            newFilename = urlDownloadLink;
        }
        return newFilename;
    }

}