package com.kumar.raman.shrikrishan.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kumar.raman.shrikrishan.Adapter.NewGalleryAdapter;
import com.kumar.raman.shrikrishan.R;

import java.util.ArrayList;
import java.util.List;

public class NewGalleryActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ProgressDialog progress;
    private NewGalleryAdapter mAdapter;
    private List<String> gallerySection = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_gallery);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Pictures By Parts");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL));

        progress = new ProgressDialog(this);
       // getAllImages();
        gallerySection.add("Shri krishan kishor roop");
        gallerySection.add("Shri krishan bal roop");
        gallerySection.add("Shri krishan Varnan");
        gallerySection.add("Shri krishan katha");
        gallerySection.add("Yashoda Maa");
        gallerySection.add("Shri Krishan Lila");
        gallerySection.add("Shri Radha Krishan Prem Lila");
        gallerySection.add("Shri Krishan Nav Roop");
        gallerySection.add("Shri krishan balram");
        gallerySection.add("Shri krishan Gita");
        gallerySection.add("Shri krishan Narayan Roop");
        gallerySection.add("Avtars");
        mAdapter = new NewGalleryAdapter(gallerySection,this,getSupportFragmentManager()
                .beginTransaction());
        recyclerView.setAdapter(mAdapter);
    }

    public void showProgressDialog(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
}
