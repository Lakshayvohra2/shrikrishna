package com.kumar.raman.shrikrishan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.io.IOException;
import java.net.URL;

/**
 * Created by mann on 1/2/18.
 */

public class ShareImageFragment extends Fragment {
    ProgressDialog progress;

    private static String TAG = ShareImageFragment.class.getName();

    private CallbackManager callbackManager;

    ShareDialog shareDialog;
    Integer imageId;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialize facebook SDK.
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        // Create a callbackManager to handle the login responses.
        callbackManager = CallbackManager.Factory.create();
        // this part is optional
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_share, container, false);
        ShareButton shareButton = (ShareButton) v.findViewById(R.id.fb_share_button);
        progress = new ProgressDialog(getActivity());
        showProgressDialog();
        if (getArguments() != null) {
            String image = getArguments().getString("image");

            setImageShare(image);
        }
//        shareButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                    setImageShare(v);
//
//            }
//        });
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setImageShare(String image) {
        Bitmap imageBitmap =null;
        try {
            URL url = new URL(image);
            imageBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(IOException e) {
            System.out.println(e);
        } catch (Exception e){
            e.printStackTrace();
        }
        if (ShareDialog.canShow(ShareLinkContent.class)){
            shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, callback);
      //  Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.bg1);
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(imageBitmap)
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            shareDialog.show(content);
    }else {
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("hi")
                .setContentDescription("hi")
                .build();
        shareDialog.show(linkContent);
    }
        progress.dismiss();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Call callbackManager.onActivityResult to pass login result to the LoginManager via callbackManager.
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            Log.v(TAG, "Successfully posted");
            // Write some code to do some operations when you shared content successfully.
        }

        @Override
        public void onCancel() {
            Log.v(TAG, "Sharing cancelled");
            // Write some code to do some operations when you cancel sharing content.
        }

        @Override
        public void onError(FacebookException error) {
            Log.v(TAG, error.getMessage());
            // Write some code to do some operations when some error occurs while sharing content.
        }
    };
    public void showProgressDialog(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
}
