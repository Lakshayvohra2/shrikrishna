package com.kumar.raman.shrikrishan.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kumar.raman.shrikrishan.Pojo.AudioModel;
import com.kumar.raman.shrikrishan.R;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Dell- on 8/4/2018.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {
    FragmentTransaction ft;
    private List<AudioModel> videoList;
    ProgressDialog progressDialog;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView video;
        TextView title;
        private ItemClickListener clickListener;

        public MyViewHolder(View view) {
            super(view);
            video = (ImageView) view.findViewById(R.id.video);
            title=(TextView)view.findViewById(R.id.title);
            video.setOnClickListener(this);
        }

        public void setClickListener(VideoAdapter.ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getPosition(), false);
        }
    }

    public VideoAdapter(List<AudioModel> arrayList, Context context, FragmentTransaction ft) {
        this.videoList = arrayList;
        this.mContext = context;
        this.ft = ft;
    }
    @Override
    public VideoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_adapter_lay, parent, false);
        return new VideoAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final String videoUrl = videoList.get(position).getUrl();
        holder.title.setText(Html.fromHtml(videoList.get(position).getTitle()));
        try {
          //  Bitmap image=retriveVideoFrameFromVideo(videoUrl);
           // Bitmap image= ThumbnailUtils.createVideoThumbnail(videoUrl, MediaStore.Video.Thumbnails.MINI_KIND);

            long interval = 5000 * 1000;
            String[] parts = videoUrl.split(Pattern.quote("?v="));
            String part1 = parts[0]; // 004
            String part2 = parts[1];
            String imgUrl="https://img.youtube.com/vi/"+part2+"/0.jpg";
            Glide.with(mContext)
                    .load(imgUrl)
                    //.placeholder(Color.BLUE)
                    //.crossFade()
                    .into(holder.video);
//            RequestOptions options = new RequestOptions().frame(interval);
//            Glide.with(mContext).asBitmap()
//                    .load(imgUrl)
//                    .apply(options)
//                    .into(holder.video);
           // holder.video.setImageBitmap(image);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        holder.video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(videoUrl));
                try {
                String[] parts = videoUrl.split(Pattern.quote("?v="));
                String part1 = parts[0]; // 004
                String part2 = parts[1];
                Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + part2));


                    mContext.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    mContext.startActivity(webIntent);
                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return videoList.size();
    }
    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

}