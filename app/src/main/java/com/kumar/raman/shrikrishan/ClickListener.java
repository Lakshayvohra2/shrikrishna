package com.kumar.raman.shrikrishan;

/**
 * Created by mann on 23/2/18.
 */

public interface ClickListener {

    void onPositionClicked(int position);

    void onLongClicked(int position);
}
