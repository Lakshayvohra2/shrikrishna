package com.kumar.raman.shrikrishan;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.kumar.raman.shrikrishan.Pojo.AratiData;
import com.kumar.raman.shrikrishan.networking.NetworkingCallbackInterface;
import com.kumar.raman.shrikrishan.networking.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TextActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private TextAdapter mAdapter;
    private List<String> text= new ArrayList<>();
    ArrayList<AratiData> arrayList=new ArrayList<>();
    String url="http://ramankumarynr.com/api/get_category_posts/?id=4";
    ProgressDialog progress;
    TextView titleText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        titleText=(TextView)findViewById(R.id.titleText);
        setTitle("Arati");


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        progress = new ProgressDialog(this);
if(isNetworkConnected()){
    getText();
}else{
    Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();

}

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    public void getText() {

        JSONObject json =new JSONObject();
        showProgressDialog();

        RequestHandler.getAllText(this,json, new NetworkingCallbackInterface() {
            @Override
            public void onSuccess(NetworkResponse response, boolean fromCache) {
                System.out.print("response........"+response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    System.out.print("jsonArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(String response, boolean fromCache) {
                System.out.print("response........"+response);
                progress.dismiss();
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("posts");
                    AratiData content=new AratiData();
for(int i=0;i<jsonArray.length();i++){
     content=new AratiData();
   JSONObject json= jsonArray.getJSONObject(i);
    content.setTitle(json.getString("title"));
   content.setContent(json.getString("content"));;
    arrayList.add(content);
}
                    mAdapter = new TextAdapter(arrayList,getApplicationContext(),getSupportFragmentManager().beginTransaction());
                    recyclerView.setAdapter(mAdapter);
                    System.out.print("jsonObjectArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(VolleyError error) {
progress.dismiss();
                Toast.makeText(getApplicationContext(), "Connection not available",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNetworkFailure(String error) {
progress.dismiss();
                Toast.makeText(getApplicationContext(), "Internet connection not available",Toast.LENGTH_SHORT).show();

            }

        });
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public void showProgressDialog(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");

        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
}
