package com.kumar.raman.shrikrishan.Adapter;

import android.app.ProgressDialog;
import android.content.Context;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kumar.raman.shrikrishan.Activity.GalleryActivity1;
import com.kumar.raman.shrikrishan.R;

import java.util.List;

/**
 * Created by Dell- on 6/6/2018.
 */


public class NewGalleryAdapter extends RecyclerView.Adapter<NewGalleryAdapter.MyViewHolder>{
    FragmentTransaction ft;
    private List<String> imageList;
    ProgressDialog progressDialog;


    Context mContext;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textview1;
        ImageView imageView;
        private ItemClickListener clickListener;

        public MyViewHolder(View view) {
            super(view);

            textview1 = (TextView) view.findViewById(R.id.title);
            imageView=(ImageView) view.findViewById(R.id.wallpaper_image);
            imageView.setOnClickListener(this);
        }

        public void setClickListener(NewGalleryAdapter.ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getPosition(), false);
        }
    }


    public NewGalleryAdapter(List<String> arrayList, Context context, FragmentTransaction ft) {
        this.imageList = arrayList;
        this.mContext = context;
        this.ft=ft;
    }

    @Override
    public NewGalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_gallery_adapter, parent, false);
        return new NewGalleryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final String title= imageList.get(position);
        holder.textview1.setText(title);


      final String url= getUrl(position);
holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
Intent i=new Intent(mContext, GalleryActivity1.class);
i.putExtra("url",url);
i.putExtra("title",title);
mContext.startActivity(i);


            }
        });


    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }
    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

public String getUrl(int position){
        String url="";
    if(position==0){
        url="https://ramankumarynr.com/api/?json=get_post&id=1250&count=500";

    }else if(position==1){
        url="https://ramankumarynr.com/api/?json=get_post&id=1248&count=500";

    }else if(position==2){
        url="https://ramankumarynr.com/api/?json=get_post&id=1246&count=500";

    }else if(position==3){
        url="https://ramankumarynr.com/api/?json=get_post&id=1244&count=500";

    }else if(position==4){
        url="https://ramankumarynr.com/api/?json=get_post&id=1242&count=500";

    }else if(position==5){
        url="https://ramankumarynr.com/api/?json=get_post&id=1271&count=500";

    }else if(position==6){
        url="https://ramankumarynr.com/api/?json=get_post&id=1273&count=500";

    }else if(position==7){
        url="https://ramankumarynr.com/api/?json=get_post&id=1275&count=500";

    }else if(position==8){
        url="https://ramankumarynr.com/api/?json=get_post&id=1277&count=500";

    }else if(position==9){
        url="https://ramankumarynr.com/api/?json=get_post&id=1278&count=500";

    }else if(position==10){
        url="https://ramankumarynr.com/api/?json=get_post&id=1603&count=500";
    }else if(position==11){
        url="https://ramankumarynr.com/api/?json=get_post&id=1599&count=500";
    }
    return url;
}
}