package com.kumar.raman.shrikrishan;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kumar.raman.shrikrishan.Pojo.AratiData;

import java.util.List;

/**
 * Created by mann on 15/2/18.
 */


public class TextAdapter extends RecyclerView.Adapter<TextAdapter.MyViewHolder>{
    FragmentTransaction ft;
    private List<AratiData> aratiList;
    Context mContext;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textview1;
        LinearLayout mainLay;
        private ItemClickListener clickListener;
        public MyViewHolder(View view) {
            super(view);
            textview1 = (TextView) view.findViewById(R.id.textview1);
            mainLay=(LinearLayout)view.findViewById(R.id.mainLay);
        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }
    }

    public TextAdapter(List<AratiData> arrayList, Context context, FragmentTransaction ft) {
        this.aratiList = arrayList;
        this.mContext = context;
        this.ft=ft;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.textview1.setText(aratiList.get(position).getTitle());
        holder.mainLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mContext,FullScreenTextActivity.class);
                i.putExtra("content",aratiList.get(position).getContent());
                i.putExtra("title",aratiList.get(position).getTitle());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return aratiList.size();
    }
    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }


}