package com.kumar.raman.shrikrishan;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.arges.sepan.argmusicplayer.Enums.ErrorType;
import com.arges.sepan.argmusicplayer.IndependentClasses.Arg;
import com.arges.sepan.argmusicplayer.IndependentClasses.ArgAudio;
import com.arges.sepan.argmusicplayer.IndependentClasses.ArgAudioList;
import com.arges.sepan.argmusicplayer.PlayerViews.ArgPlayerFullScreenView;
import com.arges.sepan.argmusicplayer.PlayerViews.ArgPlayerLargeView;
import com.arges.sepan.argmusicplayer.PlayerViews.ArgPlayerSmallView;
import com.kumar.raman.shrikrishan.Activity.FullScreenPlayerActivity;
import com.kumar.raman.shrikrishan.Pojo.AudioModel;
import com.kumar.raman.shrikrishan.networking.NetworkingCallbackInterface;
import com.kumar.raman.shrikrishan.networking.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BhajanFragment extends Fragment implements BhajanAdapter.CallAudioField{

    RecyclerView recyclerView;
    private BhajanAdapter mAdapter;
    //private List<AudioModel> audioList=new ArrayList<>();
    Spinner selctOptions;
    LinearLayout uploadAudio;
    ImageView imagePreview;
    private MediaRecorder mRecorder = null;
    static private MediaPlayer mPlayer;
    String uploadedAudioName = "";
    public ArrayList<String> audioURLList = new ArrayList<>();
    final private  ArrayList<Integer> resID = new ArrayList<>();
//    MediaPlayer mediaPlayer;
    String URL = "https://ramankumarynr.com/api/?json=get_post&id=1294";
    ProgressDialog progress;
    ImageButton play_rec;
   // ArgAudioList playlist = new ArgAudioList(true);
    public static ArgPlayerLargeView musicPlayer;
    ArgAudioList playlist = new ArgAudioList(true);
    // ArrayList<AudioModel> audioList;
    private List<AudioModel> audioList=new ArrayList<>();

   // ArgPlayerSmallView musicPlayer;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.bhajan_fragment, container, false);
        //if(musicPlayer==null) {
            musicPlayer = (ArgPlayerLargeView) v.findViewById(R.id.argmusicplayer);
            musicPlayer.enableNotification(BhajanFragment.class);

        //}
        musicPlayer.setOnErrorListener(new Arg.OnErrorListener() {
            @Override
            public void onError(ErrorType errorType, String description) {
          //      tvError.setText("Error:\nType: "+errorType+"\nDescription: "+description);
            }
        });
        musicPlayer.setOnPlaylistAudioChangedListener(new Arg.OnPlaylistAudioChangedListener() {
            @Override
            public void onPlaylistAudioChanged(ArgAudioList playlist, int currentAudioIndex) {
            //    tvMusicType.setText(String.format("PLAYLIST Audio%d: %s", playlist.getCurrentIndex(), playlist.getCurrentAudio().getTitle()));
            }
        });
       // musicPlayer.play(audioFile);
 //       Intent intent =new Intent(getActivity(),FullScreenPlayerActivity.class);
//
//        startActivityForResult(intent,100);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        uploadAudio = (LinearLayout) v.findViewById(R.id.uploadAudio);
        progress=new ProgressDialog(getActivity());

        // setTitle("Audios");

        mAdapter = new BhajanAdapter(new ArrayList<AudioModel>(), getActivity(), getActivity().getSupportFragmentManager().beginTransaction(), resID, new MediaPlayer(), new ClickListener() {
            @Override
            public void onPositionClicked(int position) {
            }

            @Override
            public void onLongClicked(int position) {
            }
        });
        //  mAdapter.setOnCallAudioFieldListener(this);
        if(isNetworkConnected()) {
            getAllAudios(this);
        }else {
            Toast.makeText(getActivity(), "Internet connection not available",Toast.LENGTH_SHORT).show();

        }


//        uploadAudio.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //  showCustomDialog();
//
//            }
//        });

        return v;
    }






//    @Override
//    public void onPause() {
//        super.onPause();
//        if (musicPlayer != null) {
//            musicPlayer.pause();
//            musicPlayer.stop();
////            mediaPlayer = null;
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (musicPlayer != null) {
            musicPlayer.pause();
            musicPlayer.stop();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                musicPlayer.restoreDefaultFocus();
            }
            //  musicPlayer.stop();
  //          mediaPlayer = null;
        }
    }


    @Override
    public void play(int positon) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void showPlayDialog(final int position) {
   //     musicPlayer.play(playlist.get(position));
//        getActivity().stopService(new Intent(getActivity(),Music.class));
//
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.audio_dialog);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        Button okButton = (Button) dialog.findViewById(R.id.okButton);
//        Button cancelButton = (Button) dialog.findViewById(R.id.cancelButton);
//        ImageButton stop_rec = (ImageButton) dialog.findViewById(R.id.stop_audio);
//        final TextView bufferingText=(TextView)dialog.findViewById(R.id.bufferingText);
//        play_rec = (ImageButton) dialog.findViewById(R.id.play_rec);
//        final Integer audio = 0;
//        play_rec.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isNetworkConnected()) {
//                    try {
//                        play_rec.setImageDrawable(getResources().getDrawable(R.drawable.pause_icon));
//
////                        Thread t = new Thread(){
////                            public void run(){
////                                Intent serviceIntent = new Intent(getActivity(), Music.class);
////                                serviceIntent.putExtra("ServiceFileDescriptor", audioList.get(position).getUrl());
////                                getActivity().startService(serviceIntent);
////                                // music.play();
////                            }
////                        };
////                        t.start();
//                       // musicPlayer.setPlaylistRepeat(true);
//                        musicPlayer.play(playlist.getCurrentAudio());
//                        //musicPlayer.playPlaylist(playlist);
//                        bufferingText.setVisibility(View.VISIBLE);
////                        if (mediaPlayer == null) {
////                            mediaPlayer = new MediaPlayer();
////                            if (!mediaPlayer.isPlaying()) {
////                                new Player().execute(audioList.get(position).getUrl());
////                            }
////                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }else{
//                    Toast.makeText(getActivity(), "Internet connection not available",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//        stop_rec.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bufferingText.setVisibility(View.GONE);
//                getActivity().stopService(new Intent(getActivity(),Music.class));
//                    play_rec.setImageDrawable(getResources().getDrawable(R.drawable.play_icon));
//
////                if (mediaPlayer != null) {
////                    if (mediaPlayer.isPlaying()) {
////                        mediaPlayer.stop();
////                        mediaPlayer=null;
////                    }
////                    play_rec.setImageDrawable(getResources().getDrawable(R.drawable.play_icon));
////                }
//            }
//        });
//        dialog.show();
//        okButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        cancelButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                bufferingText.setVisibility(View.GONE);
//                getActivity().stopService(new Intent(getActivity(),Music.class));
//                    play_rec.setImageDrawable(getResources().getDrawable(R.drawable.play_icon));
//                mediaPlayer = null;
//            }
//        });
    }

    @Override
    public ProgressDialog showProgress() {
        ProgressDialog dialog=new ProgressDialog(getActivity());
        //  progressDialog.setTitle("Loading");
        dialog.setMessage("Wait while setting ringtone/alarm");
        // progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        dialog.show();
        return dialog;
    }

    @Override
    public void dismissProgressDialog(ProgressDialog progressDialog) {
        progressDialog.dismiss();
    }

    @Override
    public void openMediaPlayer(int position) {
//        Intent intent =new Intent(getActivity(),FullScreenPlayerActivity.class);
//    //    intent.("playlist",playlist);
//        intent.putExtra("list",(Serializable) audioList);
//        intent.putExtra("position",position);
//        startActivity(intent);

        musicPlayer.setPlaylistRepeat(true);
        musicPlayer.enableNotification(MainActivity.class);
        musicPlayer.disableErrorView();


            musicPlayer.loadPlaylist(playlist);
            musicPlayer.playPlaylistItem(position);

        //if(!musicPlayer.isPlaying()) {

        //}
    }

    public void showProgressDialog(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }
    public void getAllAudios() {
        JSONObject json =new JSONObject();
        showProgressDialog();
        RequestHandler.getAllAudios(URL,getActivity(),json, new NetworkingCallbackInterface() {
            @Override
            public void onSuccess(NetworkResponse response, boolean fromCache) {
                System.out.print("response........"+response);
                progress.dismiss();
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    System.out.print("jsonArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(String response, boolean fromCache) {
                System.out.print("response........"+response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONObject json = jsonObject.getJSONObject("post");
                    AudioModel content=new AudioModel();
                    progress.dismiss();


                    JSONArray arrayAttachment=json.getJSONArray("attachments");
                    for(int j=0;j<arrayAttachment.length();j++) {
                        JSONObject attachJson= arrayAttachment.getJSONObject(j);

                        content=new AudioModel();
                        String url= attachJson.getString("url");
                        String title= attachJson.getString("title");
                        int id= attachJson.getInt("id");
                        content.setTitle(title);
                        content.setUrl(url);
                        content.setId(id);
                        audioList.add(content);
                        ArgAudio audioUrl = ArgAudio.createFromURL(title,title,url);
                        playlist.add(audioUrl);
                    }
                    Collections.sort(audioList);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(VolleyError error) {
                progress.dismiss();
                Toast.makeText(getActivity(), "Connection not available",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNetworkFailure(String error) {
                progress.dismiss();
                Toast.makeText(getActivity(), "Internet connection not available",Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void getAllAudios(final BhajanAdapter.CallAudioField a) {
        JSONObject json =new JSONObject();
        showProgressDialog();
        RequestHandler.getAllAudios(URL,getActivity(),json, new NetworkingCallbackInterface() {
            @Override
            public void onSuccess(NetworkResponse response, boolean fromCache) {
                System.out.print("response........"+response);
                progress.dismiss();
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    System.out.print("jsonArrayresponse........"+jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(String response, boolean fromCache) {
                System.out.print("response........"+response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONObject json = jsonObject.getJSONObject("post");
                    AudioModel content=new AudioModel();


                    JSONArray arrayAttachment=json.getJSONArray("attachments");
                    for(int j=0;j<arrayAttachment.length();j++) {
                        JSONObject attachJson= arrayAttachment.getJSONObject(j);

                        content=new AudioModel();
                        String url= attachJson.getString("url");
                        String title= attachJson.getString("title");
                        int id= attachJson.getInt("id");
                        content.setTitle(title);
                        content.setUrl(url);
                        content.setId(id);
                        audioList.add(content);
                        ArgAudio audioUrl = ArgAudio.createFromURL(title,"",url);
                        playlist.add(audioUrl);
                    }
                    Collections.sort(audioList);
                    mAdapter = new BhajanAdapter(audioList,getActivity() , getActivity().getSupportFragmentManager().beginTransaction(), resID, new MediaPlayer(), new ClickListener() {
                        @Override
                        public void onPositionClicked(int position) {
                        }

                        @Override
                        public void onLongClicked(int position) {
                        }
                    });
                    mAdapter.setOnCallAudioFieldListener(a);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                    recyclerView.setAdapter(mAdapter);
                   // musicPlayer.loadPlaylist(playlist);
                    //musicPlayer.playPlaylistItem(position);
                    progress.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(VolleyError error) {
                progress.dismiss();
                Toast.makeText(getActivity(), "Connection not available",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNetworkFailure(String error) {
                progress.dismiss();
                Toast.makeText(getActivity(), "Internet connection not available",Toast.LENGTH_SHORT).show();

            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100){
            getFragmentManager().popBackStack();
        }
    }
}
