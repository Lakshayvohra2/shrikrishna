package com.kumar.raman.shrikrishan.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kumar.raman.shrikrishan.PhotoFullPopupWindow;
import com.kumar.raman.shrikrishan.Pojo.ImagesData;
import com.kumar.raman.shrikrishan.R;
import com.kumar.raman.shrikrishan.ShareImageFragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by mann on 20/2/18.
 */

public class WallpaperAdapter extends RecyclerView.Adapter<WallpaperAdapter.MyViewHolder>{
    FragmentTransaction ft;
    private List<ImagesData> imageList;
    private Activity activity;
    FragmentManager fragmentManager;
    ShareFacebook shareFacebook;
    ProgressDialog newProgressDialog;
    // Progress Dialog
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    Context mContext;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textview1;
        ImageView imageView;
        private WallpaperAdapter.ItemClickListener clickListener;
        public MyViewHolder(View view) {
            super(view);

            textview1 = (TextView) view.findViewById(R.id.title);
            imageView=(ImageView) view.findViewById(R.id.wallpaper_image);
        }

        public void setClickListener(WallpaperAdapter.ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }


    }
    public void setImageListner(WallpaperAdapter.ShareFacebook shareFacebook) {
        this.shareFacebook= shareFacebook;
    }

    public WallpaperAdapter(List<ImagesData> arrayList, Context context, FragmentTransaction ft,FragmentManager fragmentManager) {
        this.imageList = arrayList;
        this.mContext = context;
        this.ft=ft;
        this.fragmentManager=fragmentManager;
        this.activity=activity;
    }

    @Override
    public WallpaperAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wallpaper_layout, parent, false);
        return new WallpaperAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
      final String images=imageList.get(position).getFullImage();
      final String title= imageList.get(position).getThumbnal();
        Picasso.with(mContext)
                .load(images)
                .into(holder.imageView);
        holder.textview1.setText(Html.fromHtml(title));
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PhotoFullPopupWindow(mContext, R.layout.popup_photo_full, view, images, null);

            }
        });
        holder.imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                showDialog(images,title);
                return true;
            }
        });
    }
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (mContext.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
    @Override
    public int getItemCount() {
        return imageList.size();
    }
    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

    public void showDialog(final String images,final String title){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_downloadshare);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
              final ImageView download=(ImageView)dialog.findViewById(R.id.download);
              final ImageView shareButton = (ImageView)dialog.findViewById(R.id.fb_share_button);
        //dialog.setCancelable(true);

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(isStoragePermissionGranted()){
                   // downloadImage(images,title);
//                    if(newProgressDialog!=null) {
//                        newProgressDialog.dismiss();
//                    }
                    shareFacebook.downloadImageFromURL(images,title);
                    //String images, final String title
                    dialog.dismiss();
                }
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
//                fragmentManager.beginTransaction()
//                        .replace(R.id.wallcontainer, new ShareImageFragment())
//                        .commit();
               newProgressDialog=shareFacebook.showProgress();
                shareFacebook.shareImage(images,title,newProgressDialog);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public interface ShareFacebook {
        public void shareImage(String image,String title,ProgressDialog dialog);
        public ProgressDialog showProgress();
        public void dismissProgressDialog(ProgressDialog progressDialog);
        public ProgressDialog showDialoadingProgress();
        public void downloadImageFromURL(String image,String title);

    }

    public void downloadImage(String images, final String title){
        Picasso.with(mContext)
                .load(images)
                .into(new Target() {
                          @Override
                          public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                              try {
                                  String root = Environment.getExternalStorageDirectory().toString();
                                  File myDir = new File(root + "/srikrishna/images");

                                  if (!myDir.exists()) {
                                      myDir.mkdirs();
                                  }

                                  String name = new Date().toString() + ".jpg";
                                  myDir = new File(myDir, name);
                                  FileOutputStream out = new FileOutputStream(myDir);
                                  bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

                                  out.flush();
                                  out.close();

                                  MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap, title , title);
                                  try {
                                      new Handler().postDelayed(new Runnable() {
                                          @Override
                                          public void run() {
                                              Toast.makeText(mContext, " Download completed", Toast.LENGTH_SHORT).show();
                                          }
                                      }, 4000);
                                  }catch (Exception e){
                                      e.printStackTrace();

                                  }
                              } catch(Exception e){
                                  e.printStackTrace();
                                  // some action
                              }
                          }

                          @Override
                          public void onBitmapFailed(Drawable errorDrawable) {
                              Toast.makeText(mContext, " Download failed", Toast.LENGTH_SHORT).show();

                              //progressDialog.dismiss();
                          }

                          @Override
                          public void onPrepareLoad(Drawable placeHolderDrawable) {
                              Toast.makeText(mContext, " Download failes", Toast.LENGTH_SHORT).show();

                          }
                      }
                );
    }


}